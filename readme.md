# Nginx Log Parser

This is a tool that parses Nginx HTTP logs and gives it to you in a more digestible way. It supports outputting the logs as JSON or CSV format.

It is written in Deno to make it easy to run.

## Prerequisites

You must have the runtime [Deno](https://deno.land/) installed on your computer.
But it's awesome. So you should have it installed anyway 😉

## Examples

### Running locally from working folder

Here we read the access log and pipe it to the program. The flag `--csv` (you can use `-c` for short) will output a comma separated format. Then we write it to a file.

```bash
cat access.log | deno run --allow-read src/bin.ts --csv > access.csv
```

### Running from remote repository

With Deno you can run programs via the http protocol. Here we use the raw bin file in the repository and provide the log as second argument.

```bash
deno run --allow-read https://gitlab.com/conqr-public/cli/nginx-log-parser/-/raw/master/src/bin.ts access.log
```

This allows running it without installing or downloading the repository.

One way of consuming the program easier could be to just alias the program in your `.bashrc` or `.zshrc` to something short and rememberable.

### Installing with Deno

If you want to install the script you can run this command. Now you run the program by using the provided name. In the example below we're naming the program `nginx-log-parser`

```bash
deno install --allow-read --name nginx-log-parser https://gitlab.com/conqr-public/cli/nginx-log-parser/-/raw/master/src/bin.ts
```
