/**
 *
 * @param filter Function to use as filter. Will be run on every item in the array.
 * @param list Array to filter and map
 *
 * Will run the function on every item in the array, like a map, and return a new array.
 * Difference being it will filter out items where the function returns `undefined`.
 * Remember that javascript returns `undefined` when nothing is returned.
 */
const filterMap = <T, K>(
  filter: (value: T) => K | undefined,
  list: T[]
): K[] => {
  return list.reduce((acc: K[], curr) => {
    const item = filter(curr)

    if (item !== undefined) {
      acc.push(item)
    }

    return acc
  }, [])
}

export { filterMap }
