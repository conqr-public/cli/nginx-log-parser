import {
  ipFromLine,
  dateFromLine,
  resourceFromLine,
  statusFromLine,
  verbFromLine,
  bytesSentFromLine,
  userAgentFromLine,
} from '../mod.ts'

interface LogLine {
  ip: string
  date: string
  verb: string
  resource: string
  status: string
  userAgent: string
  bytesSent: string
}

const parseLine = (line: string): LogLine => ({
  ip: ipFromLine(line),
  date: dateFromLine(line),
  resource: resourceFromLine(line),
  status: statusFromLine(line),
  bytesSent: bytesSentFromLine(line),
  verb: verbFromLine(line),
  userAgent: userAgentFromLine(line),
})

export { parseLine, LogLine }
