const bytesSentFromLine = (line: string): string => {
  try {
    return line.split('"')[2].split(' ')[2] ?? ''
  } catch {
    return ''
  }
}

export { bytesSentFromLine }
