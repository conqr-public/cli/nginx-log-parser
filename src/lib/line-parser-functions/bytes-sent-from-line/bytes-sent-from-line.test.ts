import { bytesSentFromLine } from './mod.ts'
import { assertEquals } from 'https://deno.land/std@0.76.0/testing/asserts.ts'

Deno.test('gets bytes sent from line', () => {
  const testData = [
    {
      text:
        '71.25.13.201 - - [17/Oct/2020:14:59:03 +0200] "GET /images/pic.img HTTP/1.1" 200 2 "-" "curl/7.64.0"',
      expected: '2',
    },
    {
      text:
        '55.48.137.88 - - [22/Oct/2020:13:30:43 +0200] "GET /path/to/file.json HTTP/1.1" 200 2333 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36"',
      expected: '2333',
    },
  ]

  testData.forEach(({ text, expected }) =>
    assertEquals(bytesSentFromLine(text), expected)
  )
})
