const resourceFromLine = (line: string): string => {
  try {
    return line.split('"')[1].split(' ').slice(1, -1).join('') ?? ''
  } catch {
    return ''
  }
}

export { resourceFromLine }
