const verbFromLine = (line: string): string => {
  try {
    return line.split(']')[1].split(' ')[1].replace('"', '') ?? ''
  } catch {
    return ''
  }
}

export { verbFromLine }
