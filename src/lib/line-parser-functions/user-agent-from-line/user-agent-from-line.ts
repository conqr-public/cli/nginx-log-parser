const userAgentFromLine = (line: string): string => {
  try {
    return line.split('"')[5] ?? ''
  } catch {
    return ''
  }
}

export { userAgentFromLine }
