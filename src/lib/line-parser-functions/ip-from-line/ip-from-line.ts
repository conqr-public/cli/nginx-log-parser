const ipFromLine = (line: string): string => {
  return line.split(' ')[0] ?? ''
}

export { ipFromLine }
