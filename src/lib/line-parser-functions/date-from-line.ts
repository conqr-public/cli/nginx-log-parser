const dateFromLine = (line: string): string => {
  try {
    const [date, ...time] =
      line.split('[')[1].split(']')[0].split(':') ?? []

    return [date, time.join(':')].join(' ') ?? ''
  } catch {
    return ''
  }
}

export { dateFromLine }
