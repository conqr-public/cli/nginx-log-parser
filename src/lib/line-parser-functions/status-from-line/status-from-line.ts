const statusFromLine = (line: string): string => {
  try {
    return line.split('"')[2].split(' ')[1] ?? ''
  } catch {
    return ''
  }
}

export { statusFromLine }
