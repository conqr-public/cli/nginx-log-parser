const getLocalFile = async (path: string) => {
  const decoder = new TextDecoder('utf-8')
  const file = await Deno.readFile(path)

  return decoder.decode(file)
}

export { getLocalFile }
