const argPayload = (...params: string[]) => {
  const index = Deno.args.findIndex(
    (arg) => arg === '--delimiter' || arg === '-d'
  )

  return index < 0
    ? ''
    : index + 1 >= Deno.args.length
    ? ''
    : Deno.args[index + 1]
}

export { argPayload }
