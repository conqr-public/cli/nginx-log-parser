import { LogLine } from '../../lib.ts'

interface iConvertToCsv {
  lines: LogLine[]
  delimiter?: string
  shouldEscapeDelimiter?: boolean
}

const convertToCsv = ({
  lines = [],
  delimiter = ',',
  shouldEscapeDelimiter = false,
}: iConvertToCsv) => {
  if (lines.length < 1) {
    return ''
  }

  const formatCell = (value: string) =>
    shouldEscapeDelimiter
      ? `"${value.replaceAll(delimiter, `\\${delimiter}`)}"`
      : `"${value}"`

  const headers = [
    '"ip"',
    '"date"',
    '"verb"',
    '"resource"',
    '"status"',
    '"user-agent"',
    '"bytes-sent"',
  ].join(delimiter)

  const rows = lines.map(
    ({ ip, date, verb, resource, status, userAgent, bytesSent }) =>
      [
        formatCell(ip),
        formatCell(date),
        formatCell(verb),
        formatCell(resource),
        formatCell(status),
        formatCell(userAgent),
        formatCell(bytesSent),
      ].join(delimiter)
  )

  return [headers, ...rows].join('\r\n')
}

export { convertToCsv }
