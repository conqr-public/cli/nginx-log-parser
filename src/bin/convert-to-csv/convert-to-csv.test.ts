import { convertToCsv } from './mod.ts'
import { LogLine } from '../../lib/line-parser-functions/mod.ts'
import { assertEquals } from 'https://deno.land/std@0.76.0/testing/asserts.ts'

Deno.test('converts log object to csv format', () => {
  const input: LogLine[] = [
    {
      ip: '71.25.13.201',
      date: '17/Oct/2020 14:59:03 +0200',
      verb: 'GET',
      resource: '/images/pic.img',
      status: '200',
      userAgent: 'curl/7.64.0',
      bytesSent: '2',
    },
    {
      ip: '55.48.137.88',
      date: '22/Oct/2020 13:30:43 +0200',
      verb: 'GET',
      resource: '/path/to/file.json',
      status: '500',
      userAgent:
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36',
      bytesSent: '2333',
    },
    {
      ip: '55.48.137.88',
      date: '22/Oct/2020 13:30:43 +0200',
      verb: 'POST',
      resource: '/path/to/file.json',
      status: '404',
      userAgent:
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36',
      bytesSent: '2333',
    },
  ]

  const expected = [
    '"ip","date","verb","resource","status","user-agent","bytes-sent"',
    '"71.25.13.201","17/Oct/2020 14:59:03 +0200","GET","/images/pic.img","200","curl/7.64.0","2"',
    '"55.48.137.88","22/Oct/2020 13:30:43 +0200","GET","/path/to/file.json","500","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML\\, like Gecko) Chrome/86.0.4240.75 Safari/537.36","2333"',
    '"55.48.137.88","22/Oct/2020 13:30:43 +0200","POST","/path/to/file.json","404","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML\\, like Gecko) Chrome/86.0.4240.75 Safari/537.36","2333"',
  ].join('\r\n')

  assertEquals(
    convertToCsv({
      lines: input,
      delimiter: ',',
      shouldEscapeDelimiter: true,
    }),
    expected
  )
})
