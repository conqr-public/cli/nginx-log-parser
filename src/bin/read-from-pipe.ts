const readFromPipe = async () => {
  if (!Deno.isatty(Deno.stdin.rid)) {
    const stdin = new TextDecoder().decode(
      await Deno.readAll(Deno.stdin)
    )
    return stdin
  }

  return ''
}

export { readFromPipe }
