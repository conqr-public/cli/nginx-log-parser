interface iCreateHelp {
  description: string
  usage: string
  options: {
    name: string
    description: string
  }[]
}

const createHelp = ({ description, usage, options }: iCreateHelp) => {
  const optionString = options
    .map(({ name, description }) => `  ${name}\t\t${description}`)
    .join('\r\n')

  return `${description}

Usage:
  ${usage}

Options:
${optionString}
`
}

export { createHelp, iCreateHelp }
