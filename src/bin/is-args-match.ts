const isArgsMatch = (...matches: string[]) => {
  return Deno.args.some((arg) => matches.includes(arg))
}

export { isArgsMatch }
