import { getLocalFile } from './lib/read-file.ts'
import {
  parseLine,
  LogLine,
} from './lib/line-parser-functions/mod.ts'

const splitToLines = (text: string) => {
  return text.split(/\r?\n/)
}

export { getLocalFile, splitToLines, parseLine, LogLine }
