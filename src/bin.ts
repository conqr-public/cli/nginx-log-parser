import {
  getLocalFile,
  splitToLines,
  parseLine,
  LogLine,
} from './lib.ts'
import { filterMap } from './common/mod.ts'
import {
  argPayload,
  convertToCsv,
  readFromPipe,
  isArgsMatch,
  createHelp,
} from './bin/mod.ts'

const parseInput = async () => {
  const pipe = await readFromPipe()

  const logLines =
    pipe.length > 0
      ? splitToLines(pipe)
      : splitToLines(await getLocalFile(Deno.args[0]))

  const parsedLines = filterMap((line) => {
    if (line.trim().length > 0) {
      return parseLine(line)
    }
  }, logLines)

  return parsedLines
}

if (isArgsMatch('--help', '-h')) {
  const settings = {
    description:
      'Parses Nginx log file and outputs formats easier to work with',
    usage:
      'nginx-log-parser <file> [OPTIONS] \r\n or cat <file> | nginx-log-parser [OPTIONS]',
    options: [
      { name: '-j, --json', description: 'Outputs to json format' },
      {
        name: '-p, --pretty',
        description:
          'Prettifies json. Requires json to be selected as output',
      },
      { name: '-c, --csv', description: 'Outputs to csv format' },
      {
        name: '-d, --delimiter <CHAR>',
        description:
          'Changes the delimiter to chosen char. Requires csv to be selected as output',
      },
      {
        name: '-e, --escape',
        description:
          'Will escape the delimiter character in cells. Requires csv to be selected as output',
      },
    ],
  }

  console.log(createHelp(settings))
} else {
  const parsedLines = await parseInput()

  if (parsedLines.length > 0) {
    let output: LogLine[] | string = parsedLines

    const wantsJson = isArgsMatch('--json', '-j')
    const wantsPrettyJson = isArgsMatch('--pretty', '-p')
    const wantsCsv = isArgsMatch('--csv', '-c')
    const wantsEscapedDelimiter = isArgsMatch('--escape', '-e')
    const delimiterPayload = argPayload('-d', '--delimiter')
    const delimiter =
      delimiterPayload.length > 0 ? delimiterPayload : ','

    if (wantsCsv && !wantsJson) {
      output = convertToCsv({
        lines: parsedLines,
        delimiter,
        shouldEscapeDelimiter: wantsEscapedDelimiter,
      })
    }

    if (wantsJson) {
      output = wantsPrettyJson
        ? JSON.stringify(parsedLines, null, 2)
        : JSON.stringify(parsedLines)
    }

    console.log(output)
  } else {
    console.log('There were no logs')
  }
}
